# amigo-dev

An opinionated collection of dev dependencies for your nodejs backend

Rather than clutter up your `devDependencies` across many projects, this module
consolidates many packages so you can use the same tools and rulesets everywhere.

Includes:

* typescript
* eslint
* prettier
* jest
* babel (used for jest typescript support only)

# Install


```console
$ yarn add -D @digiresilience/amigo-dev
```

We recommend using the sister module [@digiresilience/amigo][amigo].
[amigo]: https://gitlab.com/digiresilience/link/amigo

# Usage

See [amigo-starter][amigo-starter] for a ready to go example project.

[amigo-starter]: https://gitlab.com/digiresilience/link/amigo-starter

# Credits

Copyright © 2020-present [Center for Digital Resilience][cdr]

### Contributors

|  [![Abel Luck][abelxluck_avatar]][abelxluck_homepage]<br/>[Abel Luck][abelxluck_homepage] |
|---|

[abelxluck_homepage]: https://gitlab.com/abelxluck
[abelxluck_avatar]: https://secure.gravatar.com/avatar/0f605397e0ead93a68e1be26dc26481a?s=100&amp;d=identicon

### License

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL%20v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0.en.html)

    GNU AFFERO GENERAL PUBLIC LICENSE
    Version 3, 19 November 2007

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

[cdrtech]: https://digiresilience.org/tech/
[cdr]: https://digiresilience.org
