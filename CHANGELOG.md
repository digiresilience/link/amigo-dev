# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.4.14](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.13...0.4.14) (2021-10-11)

### [0.4.13](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.12...0.4.13) (2021-10-11)

### [0.4.12](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.11...0.4.12) (2021-10-08)

### [0.4.11](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.9...0.4.11) (2021-10-08)

### [0.4.10](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.9...0.4.10) (2021-10-08)

### [0.4.9](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.8...0.4.9) (2021-05-27)

### [0.4.8](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.7...0.4.8) (2021-05-27)

### [0.4.7](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.6...0.4.7) (2021-05-27)

### [0.4.6](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.5...0.4.6) (2021-05-27)

### [0.4.5](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.4...0.4.5) (2021-05-25)

### [0.4.4](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.2...0.4.4) (2021-05-25)

### [0.4.2](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.1...0.4.2) (2021-05-03)


### Bug Fixes

* update linter rules ([e5e77f5](https://gitlab.com/digiresilience/link/amigo-dev/commit/e5e77f59c9bd557c21d3ec406a176e906ca63767))

### [0.4.1](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.4.0...0.4.1) (2021-05-03)


### Bug Fixes

* uningore jest files for packaging ([bf61a59](https://gitlab.com/digiresilience/link/amigo-dev/commit/bf61a5993c3c5a57d1947371bc8aecb1c5bc3573))

## [0.4.0](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.3.0...0.4.0) (2021-05-03)


### ⚠ BREAKING CHANGES

* bump typescript to 4.2

### Features

* bump typescript to 4.2 ([c70d581](https://gitlab.com/digiresilience/link/amigo-dev/commit/c70d5816381cb386bec5aa6ae2c0abebb5c0fcfd))

## [0.3.0](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.2.3...0.3.0) (2021-05-03)


### ⚠ BREAKING CHANGES

* bump deps

### Features

* bump deps ([d914a9b](https://gitlab.com/digiresilience/link/amigo-dev/commit/d914a9bc0e98011c25b96cf6737c2bb1109f566f))

### [0.2.3](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.2.2...0.2.3) (2020-12-02)


### Bug Fixes

* update deps ([b1e829d](https://gitlab.com/digiresilience/link/amigo-dev/commit/b1e829d2cbc3431ebde1cc61109f283659203445))

### [0.2.2](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.2.1...0.2.2) (2020-11-24)

### [0.2.1](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.2.0...0.2.1) (2020-11-21)

## [0.2.0](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.1.3...0.2.0) (2020-11-20)


### ⚠ BREAKING CHANGES

* upgrade depds, including typescript to 4.1

### Features

* upgrade depds, including typescript to 4.1 ([bbf71be](https://gitlab.com/digiresilience/link/amigo-dev/commit/bbf71bede19b8e8f9519c114004c09ec5459bdfd))

### [0.1.3](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.1.2...0.1.3) (2020-11-20)


### Features

* bump eslint rules to pickup test extension ignoring ([259e72f](https://gitlab.com/digiresilience/link/amigo-dev/commit/259e72f82a0c49079bedaf51791e72108dad143f))

### [0.1.2](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.1.1...0.1.2) (2020-11-16)


### Bug Fixes

* update tsconfig-amigo ([036db95](https://gitlab.com/digiresilience/link/amigo-dev/commit/036db9594de6a7cc0bb75d9f70c022083b73e40e))

### [0.1.1](https://gitlab.com/digiresilience/link/amigo-dev/compare/0.1.0...0.1.1) (2020-11-11)


### Bug Fixes

* Ensure jest preset is included in npm package ([77a3d91](https://gitlab.com/digiresilience/link/amigo-dev/commit/77a3d91fc76105901b3dcce93247512dfb6d5293))

## 0.1.0 (2020-11-11)

## 0.1.0 (2020-11-11)
